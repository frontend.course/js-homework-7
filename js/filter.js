
const vehicles = [
    {
        name: 'Toyota',
        description: 'Toyota is a Japanese multinational automotive manufacturer headquartered in Toyota, Aichi, Japan.',
        contentType: {
            name: 'Automobile'
        },
        locales: [
            {
                name: 'en_US',
            },
            {
                name: 'en_GB',
            }
        ]
    },
    {
        name: 'Ford',
        description: 'Ford is an American multinational automaker headquartered in Dearborn, Michigan, USA.',
        contentType: {
            name: 'Automobile'
        },
        locales: [
            {
                name: 'en_US',
            }
        ]
    },
    {
        name: 'Volkswagen',
        description: 'Volkswagen is a German multinational automotive manufacturing company headquartered in Wolfsburg, Lower Saxony, Germany.',
        contentType: {
            name: 'Automobile'
        },
        locales: [
            {
                name: 'de_DE',
            }
        ]
    }
];

console.log(filterCollection(vehicles, 'en_US Toyota', true, 'name', 'description', 'contentType.name', 'locales.name'));

function filterCollection(array, needle, strict, ...fields) {
    const needleKeywords = needle.toLowerCase().split(" ");

    return array.filter(item => {
        const matches = needleKeywords.map(keyword => {
            return fields.some(field => {
                const value = getNestedValue(item, field);
                if (typeof value === 'string') {
                    return value.toLowerCase().includes(keyword);
                } else if (Array.isArray(value)) {
                    return value.some(subItem => {
                        if (typeof subItem === 'string') {
                            return subItem.toLowerCase().includes(keyword);
                        }
                        return false;
                    });
                }
                return false;
            });
        });

        console.log(matches)

        return strict ? matches.every(Boolean) : matches.some(Boolean);
    });
}

function getNestedValue(object, keyPath) {
    return keyPath.split('.').reduce((previous, currentKey) => {
        if (Array.isArray(previous)) {
            return previous.map(item => item[currentKey]);
        }
        return previous ? previous[currentKey] : undefined;
    }, object);
}
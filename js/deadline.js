class Developer {
    constructor(name, velocity) {
        this._name = name;
        this._velocity = velocity;
    }

    get name() {
        return this._name;
    }

    get velocity() {
        return this._velocity;
    }
}

class Task {
    constructor(name, estimate) {
        this._name = name;
        this._estimate = estimate;
    }

    get name() {
        return this._name;
    }

    get estimate() {
        return this._estimate;
    }
}

createDeveloper = (name, velocity) => new Developer(name, velocity);
createTask = (name, estimate) => new Task(name, estimate);

const developers = [
    createDeveloper('John', 1),
    createDeveloper('Mary', 2),
    createDeveloper('Mike', 3),
];

const backlog = [
    createTask('Task 1', 8),
    createTask('Task 2', 15),
    createTask('Task 3', 3),
];

const deadline = new Date('2023-06-25');

function canBeDoneBeforeDeadline(backlog, developers, deadline) {
    const totalVelocity = developers.reduce((acc, dev) => acc + dev.velocity, 0);
    const totalEstimate = backlog.reduce((acc, task) => acc + task.estimate, 0);
    const totalDays = Math.ceil(totalEstimate / totalVelocity);
    const today = new Date();
    const daysLeft = Math.ceil((deadline - today) / (1000 * 60 * 60 * 24));
    let weekendDays = 0;
    for (let i = 0; i < daysLeft; i++) {
        const day = new Date(today.getFullYear(), today.getMonth(), today.getDate() + i);
        if (day.getDay() === 0 || day.getDay() === 6) {
            weekendDays++;
        }
    }

    const workDays = daysLeft - weekendDays;

    if (totalDays <= workDays) {
        console.log(`Усі завдання будуть успішно виконані за ${Math.abs(totalDays - workDays)} днів до настання дедлайну!`);
        return true;
    } else {
        console.log(`Команді розробників доведеться витратити додатково ${(totalDays - workDays) * 8} годин після дедлайну, щоб виконати всі завдання в беклозі`);

        return false;
    }
}

console.log(canBeDoneBeforeDeadline(backlog, developers, deadline));